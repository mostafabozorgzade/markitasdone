<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Markitasdone
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
		
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	</main><!-- #main -->


	<!-- Book posttype list -->
	<?php 
		$args = array(
			'post_type' => 'books',
			// 'posts_per_page' => '6',
		); 
		$books = new WP_Query($args);
		if($books->have_posts()):
	?>
	<div style="background-color:#f8f8f8; padding:100px 50px;">
	<h2>Book List</h2>
	<ul>
		<?php while($books->have_posts()) : $books->the_post(); ?>
		<li>
			<img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<h5><?php the_taxonomies(); ?></h5>
			<p><?php the_excerpt(); ?></p>
		</li>
		<?php endwhile; ?>
	</ul>
	</div>

	<?php endif; ?>

	<!-- End Book posttype list -->

<?php
get_sidebar();
get_footer();
