<?php 

// Register books author Taxonomy
function books_author_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Authors', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Author', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Author', 'text_domain' ),
		'all_items'                  => __( 'Authors', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Author', 'text_domain' ),
		'edit_item'                  => __( 'Edit Author', 'text_domain' ),
		'update_item'                => __( 'Update Author', 'text_domain' ),
		'view_item'                  => __( 'View Author', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove Authors', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Authors', 'text_domain' ),
		'search_items'               => __( 'Search Authors', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No Authors', 'text_domain' ),
		'items_list'                 => __( 'Authors list', 'text_domain' ),
		'items_list_navigation'      => __( 'Authors list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
        
	);
	register_taxonomy( 'books_author_category', array( 'books' ), $args );

}
add_action( 'init', 'books_author_taxonomy');